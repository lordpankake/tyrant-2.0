package net.minecraft.client.renderer;

import net.minecraft.client.Minecraft;
import net.minecraft.client.renderer.chunk.RenderChunk;
import net.minecraft.client.renderer.vertex.VertexBuffer;
import net.minecraft.util.BlockRenderLayer;

public class VboRenderList extends ChunkRenderContainer {
	static float fuck = 0.0002f;
	
	public void renderChunkLayer(BlockRenderLayer layer) {
		if (this.initialized) {
			for (RenderChunk renderchunk : this.renderChunks) {
				VertexBuffer vertexbuffer = renderchunk.getVertexBufferByLayer(layer.ordinal());
				GlStateManager.pushMatrix();
				float f = (float) Math.tan(System.currentTimeMillis());
				float x = (float) (-1 + (Math.random() * 2));
				float y = (float) (-1 + (Math.random() * 2));
				float z = (float) (-1 + (Math.random() * 2));
				
				
				f = (float) (Math.random() * Math.PI * fuck);

				GlStateManager.rotate(f, x, y, z);
				this.preRenderChunk(renderchunk);
				renderchunk.multModelviewMatrix();
				vertexbuffer.bindBuffer();
				this.setupArrayPointers();
				vertexbuffer.drawArrays(7);
				GlStateManager.popMatrix();
			}

			OpenGlHelper.glBindBuffer(OpenGlHelper.GL_ARRAY_BUFFER, 0);
			GlStateManager.resetColor();
			this.renderChunks.clear();
		}
	}

	private void setupArrayPointers() {
		GlStateManager.glVertexPointer(3, 5126, 28, 0);
		GlStateManager.glColorPointer(4, 5121, 28, 12);
		GlStateManager.glTexCoordPointer(2, 5126, 28, 16);
		OpenGlHelper.setClientActiveTexture(OpenGlHelper.lightmapTexUnit);
		GlStateManager.glTexCoordPointer(2, 5122, 28, 24);
		OpenGlHelper.setClientActiveTexture(OpenGlHelper.defaultTexUnit);
	}
}
