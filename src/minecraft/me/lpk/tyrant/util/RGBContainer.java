package me.lpk.tyrant.util;

public class RGBContainer {
	public float red, green, blue, alpha;

	public RGBContainer(float red, float green, float blue, float alpha) {
		this.red = red;
		this.green = green;
		this.blue = blue;
		this.alpha = alpha;
	}
}
