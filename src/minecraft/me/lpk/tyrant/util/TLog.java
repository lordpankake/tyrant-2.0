package me.lpk.tyrant.util;

import org.apache.commons.lang3.StringUtils;

import me.lpk.tyrant.util.exception.TyrantException;
import net.minecraft.client.Minecraft;
import net.minecraft.crash.CrashReport;

public class TLog {
	private static final String PREFIX = "[T] ";

	public static void info(String message) {
		System.out.println(PREFIX + message);
	}
	

	public static void section(String message) {
		int titleLength = 70;
		info(StringUtils.center(" " + message + " ", titleLength, '='));
	}

	public static void error(TyrantException e) {
		// print to console
		e.printStackTrace();
		// Create and save report
		CrashReport crash = new TCrashReport(e.getMoreDetails(), e);
		crash.makeCategory("Tyrant");
		Minecraft.displayCrashReport(crash);
	}
	
	public static void warn(TyrantException e) {
		section("WARNING-START");
		info("Warning Message: ");
		info(e.getMessage());
		info("Additional Details: ");
		info(e.getMoreDetails());
		section("WARNING-END");
	}

	private static class TCrashReport extends CrashReport {

		public TCrashReport(String desc, Throwable cause) {
			super(desc, cause);
		}

	}


}
