package me.lpk.tyrant.util;

import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.Locale;

import javax.tools.Diagnostic;
import javax.tools.DiagnosticListener;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.SimpleJavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.StandardLocation;
import javax.tools.ToolProvider;
import javax.tools.JavaCompiler.CompilationTask;
import javax.tools.JavaFileObject.Kind;

import me.lpk.tyrant.Tyrant;
import me.lpk.tyrant.util.exception.TyrantException;

/**
 * Example compiling code provided by <a href=
 * "https://stackoverflow.com/questions/40983414/dynamically-compile-java-code-which-has-dependencies-on-classes-loaded-by-specif">
 * Holger</a>
 */
public class Javac<T> {
	private final ClassDefiner definer = new ClassDefiner();
	private final JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
	/**
	 * Directory used for compiling code.
	 */
	private final File outDir = Tyrant.instance().resources.getOrMakeDirectory("bin");;
	/**
	 * Javac exception listener.
	 */
	private DiagnosticListener<JavaFileObject> diagnosticListener = new DefaultDiagnosticListener();

	/**
	 * Compile and load a class from the given source file.
	 * 
	 * @param sourceFile
	 *            Java source file.
	 * @return Class<T>
	 * @throws IOException
	 */
	public Class<T> compile(File sourceFile) throws ClassCastException, IOException {
		String content = readFile(sourceFile.getPath(), StandardCharsets.UTF_8);
		String className = getNameFromSource(content);
		return this.compile(sourceFile.getName(), className, content);
	}

	/**
	 * Compile and load a class from the given source file.
	 * 
	 * @param sourceFileName
	 *            File name of source file.
	 * @param className
	 *            Class in the file being loaded.
	 * @param classContent
	 *            Full contents of the source file.
	 * @return Class<T>
	 * @throws TyrantException
	 * @throws IOException
	 */
	public Class<T> compile(String sourceFileName, String className, String classContent)
			throws ClassCastException, IOException {
		StandardJavaFileManager fm = getJavaFileManager(Locale.ENGLISH);
		JavaCompiler.CompilationTask task = createCompileTask(fm, sourceFileName, classContent);
		// Compile.
		// If returns false, the listener will shit out some data
		if (task.call()) {
			JavaFileObject fo = fm.getJavaFileForInput(StandardLocation.CLASS_OUTPUT, className, Kind.CLASS);
			byte[] classbytes = Files.readAllBytes(Paths.get(fo.toUri()));
			return (Class<T>) definer.load(className, classbytes);
		}
		return null;
	}

	/**
	 * Create a compilation task for the given source given the source file name,
	 * source contents, and the file manager that will be used to handle compilation
	 * of the source.
	 * 
	 * @param fm
	 * @param sourceFileName
	 * @param classContent
	 * @return
	 */
	protected CompilationTask createCompileTask(StandardJavaFileManager fm, String sourceFileName,
			String classContent) {
		return compiler.getTask(null, fm, diagnosticListener, Collections.emptySet(), Collections.emptySet(),
				Collections.singleton(new SimpleJavaFileObject(URI.create("string:///" + sourceFileName), Kind.SOURCE) {
					public CharSequence getCharContent(boolean ignoreEncodingErrors) {
						return classContent;
					}
				}));
	}

	/**
	 * Create a JavaFileManager with the given Locale.
	 * 
	 * @param locale
	 * @return
	 * @throws IOException
	 */
	protected StandardJavaFileManager getJavaFileManager(Locale locale) throws IOException {
		StandardJavaFileManager fm = compiler.getStandardFileManager(diagnosticListener, locale,
				Charset.defaultCharset());
		fm.setLocation(StandardLocation.CLASS_OUTPUT, Collections.singleton(outDir));
		return fm;
	}

	/**
	 * Set javac exception listener.
	 * 
	 * @param diagnosticListener
	 */
	public void setDiagnosticListener(DiagnosticListener<JavaFileObject> diagnosticListener) {
		this.diagnosticListener = diagnosticListener;
	}

	/**
	 * Read all text from the given file.
	 * 
	 * @param path
	 * @param encoding
	 * @return
	 * @throws IOException
	 */
	protected static String readFile(String path, Charset encoding) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, encoding);
	}

	/**
	 * Forgive me father for I have sinned... But fuck I cannot find a better way to
	 * do this.
	 * 
	 * @param content
	 *            Class source.
	 * @return Quantified class name.
	 */
	protected static String getNameFromSource(String content) {
		// get package name
		String p = "package ";
		int packCut = content.indexOf(p) + p.length();
		String pack = content.substring(packCut, content.indexOf(";", packCut)).trim();
		// get class name
		String c = "class ";
		String e = "extends";
		int classCut = content.indexOf(c) + c.length();
		String clazz = content.substring(classCut, content.indexOf(e, classCut)).trim();
		// slap together, you got yourself a quantified name!
		return pack + "." + clazz;
	}

	/**
	 * ClassLoader extension to proxy-call ClassLoader.defineClass.
	 */
	public final class ClassDefiner extends ClassLoader {
		public final Class<?> load(String name, byte[] bytes) {
			return defineClass(name, bytes, 0, bytes.length, null);
		}
	}

	/**
	 * Basic listener that shits out errors to the sys-out.
	 */
	public final class DefaultDiagnosticListener implements DiagnosticListener {
		@Override
		public void report(Diagnostic diagnostic) {
			System.out.println(diagnostic.toString());
		}
	};
}
