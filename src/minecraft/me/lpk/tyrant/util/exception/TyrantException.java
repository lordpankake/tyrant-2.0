package me.lpk.tyrant.util.exception;

public class TyrantException extends Throwable {
	private final String details;

	public TyrantException(String message, String details) {
		super(message);
		this.details = details;
	}

	public String getMoreDetails() {
		return details;
	}
}
