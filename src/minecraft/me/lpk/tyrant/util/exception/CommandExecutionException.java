package me.lpk.tyrant.util.exception;

public class CommandExecutionException extends TyrantException {

	public CommandExecutionException(String message, String details) {
		super(message, details);
	}

}
