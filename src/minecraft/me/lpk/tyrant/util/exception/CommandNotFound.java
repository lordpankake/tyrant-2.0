package me.lpk.tyrant.util.exception;

public class CommandNotFound extends TyrantException {
	private final String input;
	private final String command;

	public CommandNotFound(String message, String details, String input, String command) {
		super(message, details);
		this.input = input;
		this.command = command;
	}

	/**
	 * Input text sent.
	 */
	public String getInput() {
		return input;
	}

	/**
	 * Command that was not found.
	 */
	public String getCommand() {
		return command;
	}

}
