package me.lpk.tyrant.util;

/**
 * Utility for generating and getting data from integer colors.
 */
public class Colors {
	public static int getColor(int red, int green, int blue) {
		return getColor(red, green, blue, 255);
	}

	public static int getColor(int red, int green, int blue, int alpha) {
		int color = 0;
		color |= alpha << 24;
		color |= red << 16;
		color |= green << 8;
		color |= blue;
		return color;
	}

	public static double getAlpha(int color) {
		return ((double) ((color >> 24 & 0xff) / 255F));
	}

	public static double getRed(int color) {
		return ((double) ((color >> 16 & 0xff) / 255F));
	}

	public static double getGreen(int color) {
		return ((double) ((color >> 8 & 0xff) / 255F));
	}

	public static double getBlue(int color) {
		return ((double) ((color & 0xff) / 255F));
	}
}
