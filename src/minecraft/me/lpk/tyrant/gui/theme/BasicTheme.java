package me.lpk.tyrant.gui.theme;

import me.lpk.tyrant.gui.UIScreen;
import me.lpk.tyrant.gui.component.ClickComponent;
import me.lpk.tyrant.gui.component.Component;
import me.lpk.tyrant.gui.component.Component.Direction;
import me.lpk.tyrant.gui.component.Frame;
import me.lpk.tyrant.gui.component.TextComponent;
import me.lpk.tyrant.gui.component.impl.PluginToggleButton;
import me.lpk.tyrant.util.Colors;
import net.minecraft.client.gui.Gui;
import net.minecraft.util.text.TextFormatting;

public class BasicTheme extends Theme {
	int colorBlack = Colors.getColor(0, 0, 0);;
	int colorFill = Colors.getColor(40, 45, 50);
	int colorFillButton = Colors.getColor(75, 80, 85);
	int colorBorder = Colors.getColor(30, 34, 40);
	int colorText = -1; // Colors.getColor(0, 0, 0);
	int colorFillDrag = Colors.getColor(100, 110, 115);
	int colorFillDragText = colorBlack;

	@Override
	public void onRender(UIScreen screen, Component component) {
		int left = component.getDisplayX();
		int top = component.getDisplayY();
		int right = left + component.getDisplayWidth();
		int bottom = top + component.getDisplayHeight();
		//
		int pWest = component.getPadding(Direction.WEST);
		int pNorth = component.getPadding(Direction.NORTH);
		//
		int textOffset = 2;
		//
		Gui.drawRect(left, top, right, bottom, colorFill);
		//
		if (component instanceof ClickComponent) {
			ClickComponent button = (ClickComponent) component;
			Gui.drawRect(left, top, right, bottom, colorFillButton);
			Gui.drawBorderedRect(left, top, right, bottom, 1, colorBorder);
			if (button instanceof PluginToggleButton) {
				PluginToggleButton pButton = (PluginToggleButton) button;
				int colorStatus = pButton.getPlugin().isEnabled() ? Colors.getColor(100, 255, 100)
						: Colors.getColor(255, 100, 100);
				Gui.drawRect(right - 4, top + 1, right, bottom, colorStatus);
				Gui.drawRect(right - 5, top, right - 4, bottom, colorBorder);
			}
			screen.getFontRenderer().drawString(button.getText(), left + pWest + 1, top + pNorth + textOffset + 1,
					colorBlack);
			screen.getFontRenderer().drawString(button.getText(), left + pWest, top + pNorth + textOffset, colorText);;
		} else if (component instanceof TextComponent) {
			TextComponent label = (TextComponent) component;
			String text = label.getText();
			if (label.getPropertyOrDefault("title", false)) {
				Gui.drawRect(left, top, right, bottom, colorFillDrag);
				text = TextFormatting.BOLD + text;
				screen.getFontRenderer().drawString(text, left + pWest, top + pNorth + textOffset + 1,
						colorFillDragText);
			} else {
				screen.getFontRenderer().drawString(text, left + pWest, top + pNorth + textOffset, colorText);
			}
		} else if (component instanceof Frame) {
			int dragBottom = top + ((Frame) component).getDragHeight();
			Gui.drawRect(left, top, right, dragBottom, colorFillDrag);
			Gui.drawBorderedRect(left, top, right, bottom, 1, colorBorder);
		}
		
	}
}
