package me.lpk.tyrant.gui.screen;

import java.io.IOException;

import me.lpk.tyrant.Tyrant;
import net.minecraft.client.gui.GuiButton;
import net.minecraft.client.gui.GuiIngameMenu;

public class InGameMenuHook extends GuiIngameMenu {

	@Override
	public void initGui() {
		super.initGui();

		int buttWidth = 80;
		int buttHeight = 20;
		
		this.buttonList.add(new GuiButton(11, 5, this.height - buttHeight - 5, buttWidth,buttHeight,
				Tyrant.instance().resources.translate("tyrant.reload")));
	}
	
	@Override
	protected void actionPerformed(GuiButton button) throws IOException {
		super.actionPerformed(button);
		
		if (button.id == 11) {
			Tyrant.instance().reload();
		}
	}
}
