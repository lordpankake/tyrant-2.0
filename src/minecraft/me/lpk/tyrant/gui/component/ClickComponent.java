package me.lpk.tyrant.gui.component;

import java.util.HashMap;
import java.util.Map;

/**
 * Basic clickable component.
 */
public class ClickComponent extends TextComponent {
	private final Map<Integer, ClickAction> actions = new HashMap<>();

	public ClickComponent(String text) {
		super(text);
		this.setOpaqueClick(true);
	}

	@Override
	public void onClick(int mouseX, int mouseY, int button) {
		if (actions.containsKey(button)) {
			actions.get(button).onClick(this, mouseX, mouseY, button);
		}
	}

	/**
	 * Sets the left click action when the component is clicked.
	 * 
	 * @param action
	 */
	public void setClickAction(ClickAction action) {
		setClickAction(0, action);
	}

	/**
	 * Sets the action that occurs when the component is clicked.
	 * 
	 * @param buttonID
	 *            Mouse button.
	 * 
	 * @param action
	 */
	public void setClickAction(int buttonID, ClickAction action) {
		actions.put(buttonID, action);
	}

	public static abstract class ClickAction {
		public abstract void onClick(ClickComponent caller, int mouseX, int mouseY, int button);
	}
}
