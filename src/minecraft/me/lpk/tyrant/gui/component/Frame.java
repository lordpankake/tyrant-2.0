package me.lpk.tyrant.gui.component;

import me.lpk.tyrant.gui.component.type.Draggable;

public class Frame extends Container implements Draggable {
	private final int dragHandleHeight;
	private boolean dragging;

	public Frame(int x, int y, int dragHandleHeight) {
		super(x, y, 0, 0);
		this.dragHandleHeight = dragHandleHeight;
		this.setClickThroughChildren(true);
	}

	@Override
	public void onDrag(int dx, int dy) {
		if (dragging) {
			setX(getX() + dx);
			setY(getY() + dy);
		}
	}

	@Override
	public void onClick(int mouseX, int mouseY, int button) {
		// Update draggable logic
		Draggable.super.onClick(mouseX, mouseY, button);
		// Send click to children components
		for (Component component : getChildren()) {
			if (component.isMouseOver(mouseX, mouseY)) {
				component.onClick(mouseX, mouseY, button);
			}
		}
	}

	@Override
	public boolean isInDragRegion(int mouseX, int mouseY) {
		if (mouseX >= getDisplayX() && mouseX <= getDisplayX() + getWidth()) {
			if (mouseY >= getDisplayY() && mouseY <= getDisplayY() + dragHandleHeight) {
				return true;
			}
		}
		return false;
	}

	public int getDragHeight() {
		return dragHandleHeight;
	}

	@Override
	public boolean isDragged() {
		return dragging;
	}

	@Override
	public boolean setIsDragged(boolean value) {
		return dragging = value;
	}
}
