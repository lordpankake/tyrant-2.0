package me.lpk.tyrant.gui.component.type;

import me.lpk.tyrant.gui.component.Size;

public interface LayoutComponent {
	Size getMinimumSize();
}
