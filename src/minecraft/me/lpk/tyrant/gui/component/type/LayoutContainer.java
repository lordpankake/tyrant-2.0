package me.lpk.tyrant.gui.component.type;

import java.util.Collection;

import me.lpk.tyrant.gui.component.Component;
import me.lpk.tyrant.gui.component.Component.Direction;
import me.lpk.tyrant.gui.component.Container;

public interface LayoutContainer {

	/**
	 * @return The layout for component handling.
	 */
	Layout getLayout();

	/**
	 * @return This component.
	 */
	Container getContainer();

	/**
	 * Set the layout for component handling.
	 * 
	 * @return
	 */
	void setLayout(Layout layout);

	/**
	 * @return Collection of child components.
	 */
	Collection<Component> getChildren();

	/**
	 * Dynamically adds a child-component and fits it into the current component.
	 * 
	 * @param child
	 *            Component to add.
	 * @return
	 */
	default void add(Component child) {
		child.setParent(getContainer());
		getChildren().add(child);
		getLayout().invalidate();
		getContainer().pack();
	}

	/**
	 * Dynamically adds a child-component and fits it into the current component.
	 * 
	 * @param child
	 *            Component to add.
	 * @param key
	 *            Key to associate child with in {@link #getComponent() component's}
	 *            property map.
	 */
	default void add(Component child, String key) {
		getContainer().setProperty(key, child);
		add(child);
	}

	/**
	 * Dynamically adds a child-component and fits it into the current component.
	 * 
	 * @param child
	 *            Component to add.
	 * @param key
	 *            Direction to associate with component. Used in certain layout
	 *            managers.
	 */
	default void add(Component child, Direction key) {
		add(child, key.name());
	}

	/**
	 * Dynamically removes a child-component and adjusts the others to into the
	 * current component.
	 * 
	 * @return
	 */
	default void remove(Component child) {
		getChildren().remove(child);
		getLayout().invalidate();
		getContainer().pack();
	}
}
