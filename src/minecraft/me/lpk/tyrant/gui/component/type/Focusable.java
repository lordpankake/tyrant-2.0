package me.lpk.tyrant.gui.component.type;

import me.lpk.tyrant.gui.event.Events;

public interface Focusable extends Clickable {
	default void onClick(int mouseX, int mouseY, int button) {
		Events.focus(this);
	}

	default void onGainFocus() {
		setFocused(true);
	}

	default void onLoseFocus() {
		setFocused(false);
	}

	boolean isFocused();

	void setFocused(boolean b);
}
