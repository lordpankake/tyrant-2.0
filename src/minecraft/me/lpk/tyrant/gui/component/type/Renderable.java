package me.lpk.tyrant.gui.component.type;

import me.lpk.tyrant.gui.UIScreen;
import me.lpk.tyrant.gui.theme.Theme;

public interface Renderable {
	void onRender(UIScreen screen, Theme theme);
}
