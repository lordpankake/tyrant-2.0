package me.lpk.tyrant.gui.component.type;

public interface Hoverable {
	boolean isMouseOver(int mouseX, int mouseY);
}
