package me.lpk.tyrant.gui.component.type;

public interface Draggable extends Clickable , Hoverable {
	void onDrag(int dx, int dy);
	
	boolean isInDragRegion(int mouseX, int mouseY);

	boolean isDragged();

	boolean setIsDragged(boolean value);

	@Override
	default void onClick(int mouseX, int mouseY, int button) {
		if (isInDragRegion(mouseX, mouseY)){
			setIsDragged(true);
		}
	}
	
	@Override
	default void onRelease(int mouseX, int mouseY, int button) {
		if (isInDragRegion(mouseX, mouseY)){
			setIsDragged(false);
		}
	}
}
