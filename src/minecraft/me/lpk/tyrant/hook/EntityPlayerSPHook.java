package me.lpk.tyrant.hook;

import me.lpk.event.SubscriptionManager;
import me.lpk.tyrant.Tyrant;
import me.lpk.tyrant.plugin.listener.*;
import net.minecraft.client.Minecraft;
import net.minecraft.client.entity.EntityPlayerSP;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.play.client.CPacketChatMessage;
import net.minecraft.stats.RecipeBook;
import net.minecraft.stats.StatisticsManager;
import net.minecraft.world.World;

public class EntityPlayerSPHook extends EntityPlayerSP {
	private static final SubscriptionManager events = Tyrant.instance().events;

	public EntityPlayerSPHook(Minecraft mc, World world, NetHandlerPlayClient net, StatisticsManager stats,
			RecipeBook book) {
		super(mc, world, net, stats, book);
	}

	@Override
	public void sendChatMessage(String message) {
		Messaging.EventMessageOut event = new Messaging.EventMessageOut(message);
		events.invoke(event);
		if (!event.isCancelled()) {
			this.connection.sendPacket(new CPacketChatMessage(event.message));
		}
	}

	@Override
	public void onUpdate() {
		// tick updates
		super.onUpdate();
		events.invoke(new Tickable.EventTick());
	}

	@Override
	protected void onUpdateWalkingPlayer() {
		// Motion updates
		super.onUpdateWalkingPlayer();
	}
}
