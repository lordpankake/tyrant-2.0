package me.lpk.tyrant.hook;

import com.mojang.authlib.GameProfile;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.GuiScreen;
import net.minecraft.client.network.NetHandlerPlayClient;
import net.minecraft.network.NetworkManager;

// Technically you could do everything in here by listening to packets
// but just making singular events for some of these will be more optimized
// than listening to ALL packets.
public class NetHandlerPlayClientHook extends NetHandlerPlayClient {

	public NetHandlerPlayClientHook(Minecraft mc, GuiScreen screen, NetworkManager net, GameProfile profile) {
		super(mc, screen, net, profile);
	}

}
