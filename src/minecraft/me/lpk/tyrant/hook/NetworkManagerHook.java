package me.lpk.tyrant.hook;

import io.netty.channel.ChannelHandlerContext;
import me.lpk.tyrant.Tyrant;
import me.lpk.tyrant.plugin.listener.Packets;
import net.minecraft.network.EnumPacketDirection;
import net.minecraft.network.NetworkManager;
import net.minecraft.network.Packet;

public class NetworkManagerHook extends NetworkManager {

	public NetworkManagerHook(EnumPacketDirection dir) {
		super(dir);
	}

	@Override
	public void sendPacket(Packet<?> packet) {
		Packets.EventPacketOut event = new Packets.EventPacketOut(packet);
		Tyrant.instance().events.invoke(event);
		if (!event.isCancelled()){
			super.sendPacket(packet);
		}
	}

	@Override
	protected void channelRead0(ChannelHandlerContext context, Packet<?> packet) throws Exception {
		Packets.EventPacketIn event = new Packets.EventPacketIn(packet);
		Tyrant.instance().events.invoke(event);
		if (!event.isCancelled()){
			super.channelRead0(context, packet);
		}
	}
}
