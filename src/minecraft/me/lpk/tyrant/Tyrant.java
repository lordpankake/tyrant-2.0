package me.lpk.tyrant;

import me.lpk.event.SubscriptionManager;
import me.lpk.tyrant.gui.screen.ConsoleScreen;
import me.lpk.tyrant.plugin.PluginManager;
import me.lpk.tyrant.plugin.cli.CommandManager;
import me.lpk.tyrant.util.Resources;
import me.lpk.tyrant.util.TLog;
import me.lpk.tyrant.util.exception.TyrantException;

public class Tyrant {
	private static Tyrant instance;
	public SubscriptionManager events;
	public PluginManager plugins;
	public CommandManager commands;
	public Resources resources;
	public ConsoleScreen console;


	public Tyrant() {
		TLog.section("Tyrant: Setup (START)");
		instance = this;
		resources = new Resources();
		events = new SubscriptionManager();
		events.setAnnotationStrategy(new SubscriptionManager.AnnotationDiscoveryStrategy.Parent());
		console = new ConsoleScreen();
		TLog.section("Tyrant: Setup (FINISH)");
		Runtime.getRuntime().addShutdownHook( new Thread() {
			@Override
			public void run() {
				Tyrant.instance().unload();
			}
		});
		//TEMP.exec0();
	}

	public void reload() {
		try {
			TLog.section("Tyrant: Reload (START)");
			resources.reload();
			if (plugins != null) {
				plugins.unload();
			}
			if (commands != null) {
				commands.unload();
			}
			plugins = new PluginManager();
			plugins.reload();
			commands = new CommandManager();
			commands.reload();
			TLog.section("Tyrant: Reload (FINISH)");
		} catch (TyrantException e) {
			TLog.section("Tyrant: Reload (ERROR)");
			TLog.error(e);
		}
	}

	public void unload() {
		plugins.unload();
		commands.unload();
	}

	public static Tyrant instance() {
		return instance;
	}

}
