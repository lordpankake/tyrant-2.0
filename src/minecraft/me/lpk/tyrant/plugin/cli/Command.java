package me.lpk.tyrant.plugin.cli;

import java.util.Collections;
import java.util.List;

import com.github.drapostolos.typeparser.TypeParser;

import me.lpk.tyrant.plugin.util.Console;
import me.lpk.tyrant.plugin.util.MC;

public abstract class Command implements MC, Console {
	protected final TypeParser parser = TypeParser.newBuilder().build();
	private String name;
	private String desc;
	private String[] aliases;

	public Command(String name, String desc, String... aliases) {
		this.name = name;
		this.desc = desc;
		this.aliases = new String[aliases.length + 1];
		this.aliases[0] = name;
		System.arraycopy(aliases, 0, this.aliases, 1, aliases.length);
	}

	/**
	 * Command's primary name.
	 * 
	 * @return
	 */
	public String getName() {
		return name;
	}

	/**
	 * Description of what the command does.
	 * 
	 * @return
	 */
	public String getDescription() {
		return desc;
	}

	/**
	 * Full list of aliases of the command. Index 0 will return the value returned
	 * by {@link #getName()}.
	 * 
	 * @return
	 */
	public String[] getAliases() {
		return aliases;
	}

	/**
	 * Called when Picocli finishes preparing the command instance for invocation.
	 */
	public abstract void run();

	/**
	 * Allow commands to offer suggested values for parameters.
	 * 
	 * @param paramIndex
	 *            The index requested for tab-completion.
	 * @param args
	 *            Array of command arguments.
	 * @param currentText
	 *            The current text for the index.
	 * @return
	 */
	public List<String> getCompletions(int paramIndex, String[] args, String currentText) {
		return Collections.EMPTY_LIST;
	}
}
