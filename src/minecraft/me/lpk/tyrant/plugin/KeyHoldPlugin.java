package me.lpk.tyrant.plugin;

import me.lpk.tyrant.Tyrant;

/**
 * Plugin that is enabled <i>(or disabled)</i> while a key is held down.
 */
public abstract class KeyHoldPlugin extends KeyPlugin {
	private final boolean activeOnDown;
	private boolean enabled;

	public KeyHoldPlugin(String name, String description, int key, boolean activeOnDown) {
		this(name, description, "plugin.type.hold", key, activeOnDown);
	}

	public KeyHoldPlugin(String name, String description, String category, int key, boolean activeOnDown) {
		super(name, description, category, key);
		this.activeOnDown = activeOnDown;
	}

	/**
	 * Called when {@link #getKey() the plugin key} is pressed.
	 */
	public final void onKeyDown() {
		setEnabled(activeOnDown);
	}

	/**
	 * Called when {@link #getKey() the plugin key} is released.
	 */
	public final void onKeyRelease() {
		setEnabled(!activeOnDown);
	}

	private final void setEnabled(boolean shouldActivate) {
		enabled = shouldActivate;
		if (shouldActivate) {
			Tyrant.instance().events.subscribe(this);
			this.onEnable();
		} else {
			Tyrant.instance().events.unsubscribe(this);
			this.onDisable();
		}
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void onEnable() {}

	public void onDisable() {}
}
