package me.lpk.tyrant.plugin.listener;

import me.lpk.event.SubscriptionManager.Event;
import me.lpk.event.SubscriptionManager.Subscribe;

public interface Messaging {
	@Subscribe
	void onMessageOut(EventMessageOut event);

	@Subscribe
	void onMessageIn(EventMessageIn event);

	class EventMessageOut extends Event {
		public String message;

		public EventMessageOut(String message) {
			this.message = message;
		}
	}

	class EventMessageIn extends Event {
		public String message;

		public EventMessageIn(String message) {
			this.message = message;
		}
	}
}
