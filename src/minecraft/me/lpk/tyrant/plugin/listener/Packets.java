package me.lpk.tyrant.plugin.listener;

import me.lpk.event.SubscriptionManager.Event;
import me.lpk.event.SubscriptionManager.Subscribe;
import net.minecraft.network.Packet;

public interface Packets {
	@Subscribe
	void onPacketIn(EventPacketIn event);

	@Subscribe
	void onPacketOut(EventPacketOut event);

	class EventPacketIn extends Event {
		public Packet<?> packet;

		public EventPacketIn(Packet<?> packet) {
			this.packet = packet;
		}
	}

	class EventPacketOut extends Event {
		public Packet<?> packet;

		public EventPacketOut(Packet<?> packet) {
			this.packet = packet;
		}
	}
}
