package me.lpk.tyrant.plugin.listener;

import me.lpk.event.SubscriptionManager.Event;
import me.lpk.event.SubscriptionManager.Subscribe;

public interface Renderable3D {

	@Subscribe
	void onRenderBobbing(EventRenderBobbing event);

	@Subscribe
	void onRenderNoBobbing(EventRenderNoBobbing event);

	class EventRenderBobbing extends Event {
		public final float partialTicks;

		public EventRenderBobbing(float partialTicks) {
			this.partialTicks = partialTicks;
		}
	}

	class EventRenderNoBobbing extends Event {
		public final float partialTicks;

		public EventRenderNoBobbing(float partialTicks) {
			this.partialTicks = partialTicks;
		}
	}
}
