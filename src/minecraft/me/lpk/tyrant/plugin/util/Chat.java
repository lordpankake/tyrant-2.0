package me.lpk.tyrant.plugin.util;

import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;

public interface Chat extends MC {
	/**
	 * Print a normal chat message.
	 * 
	 * @param msg
	 *            The message.
	 */
	default void info(String msg) {
		mc.player.addChatMessage(new TextComponentString(msg));
	}

	/**
	 * Print an error message to chat.
	 * 
	 * @param msg
	 *            The err message.
	 */
	default void error(String msg) {
		mc.player.addChatMessage(new TextComponentString(TextFormatting.RED + msg));
	}
}