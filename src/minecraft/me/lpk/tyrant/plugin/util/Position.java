package me.lpk.tyrant.plugin.util;

import net.minecraft.util.math.BlockPos;

public interface Position extends MC {
	/**
	 * Check if the given position is out of the player's reach.
	 * 
	 * @param pos
	 *            Position to check.
	 * @return
	 */
	default boolean outOfRange(BlockPos pos) {
		float offset = mc.playerController.isInCreativeMode() ? 1.5f : 0.5F;
		return Math.sqrt(mc.player.getDistanceSqToCenter(pos)) > mc.playerController.getBlockReachDistance() + offset;
	}

	/**
	 * Return the current player's position.
	 * 
	 * @return
	 */
	default BlockPos currentPos() {
		return mc.player.getPosition();
	}
}