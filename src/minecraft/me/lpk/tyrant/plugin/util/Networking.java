package me.lpk.tyrant.plugin.util;

import net.minecraft.network.Packet;

public interface Networking extends MC {
	/**
	 * Send a packet.
	 * 
	 * @param packet
	 *            Packet to send.
	 */
	@SuppressWarnings("all")
	default void send(Packet packet) {
		// the sendPacket with 3 params is not hooked, so using it is fine.
		mc.getConnection().getNetworkManager().sendPacket(packet, null, null);
	}
}