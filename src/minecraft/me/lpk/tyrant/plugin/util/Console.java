package me.lpk.tyrant.plugin.util;

import me.lpk.tyrant.Tyrant;
import net.minecraft.util.text.TextFormatting;

public interface Console extends MC {
	/**
	 * Print a message to console..
	 * 
	 * @param msg
	 *            The message.
	 */
	default void info(String msg) {
		Tyrant.instance().console.print(msg);
	}

	/**
	 * Print an error message to console.
	 * 
	 * @param msg
	 *            The err message.
	 */
	default void error(String msg) {
		Tyrant.instance().console.print(TextFormatting.RED +msg);
	}
}