package me.lpk.tyrant.plugin;

public abstract class KeyPlugin extends Plugin {
	private int key;

	public KeyPlugin(String name, String description, String category, int key) {
		super(name, description, category);
		this.key = key;
	}

	/**
	 * Returns the key value used for handling the state of the plugin.
	 * 
	 * @return
	 */
	public final int getKey() {
		return key;
	}

	/**
	 * Sets the key value used for handling the state of the plugin.
	 * 
	 * @param key
	 */
	public final void setKey(int key) {
		this.key = key;
	}
}
