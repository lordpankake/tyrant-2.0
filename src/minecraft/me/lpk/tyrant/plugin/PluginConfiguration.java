package me.lpk.tyrant.plugin;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

import org.apache.commons.io.FileUtils;

import com.google.gson.ExclusionStrategy;
import com.google.gson.FieldAttributes;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;
import com.google.gson.annotations.Expose;

import me.lpk.tyrant.Tyrant;
import me.lpk.tyrant.util.TLog;
import me.lpk.tyrant.util.exception.TyrantException;

/**
 * Configuration manager.
 */
public class PluginConfiguration {
	private final File confFile;
	private final Plugin plugin;

	public PluginConfiguration(Plugin plugin) {
		this.plugin = plugin;
		File confDir = Tyrant.instance().resources.getOrMakeDirectory("conf");
		confFile = new File(confDir, plugin.getID() + ".json");
		boolean exists = confFile.exists();
		if (!exists) {
			try {
				confFile.createNewFile();
				FileUtils.write(confFile, "{}");
			} catch (IOException e) {
				TLog.error(new TyrantException("Failed to create plugin settings.",
						"The client failed to create the config file for: '" + plugin.getName()
								+ "' during initialization, giving the following reason: \n" + e.getMessage()));
			}
		}
	}

	public void onUnload() {
		try {
			Writer writer = new FileWriter(confFile);
			writer.write(new GsonBuilder().setExclusionStrategies(new ExclusionStrategy() {

				@Override
				public boolean shouldSkipClass(Class<?> clazz) {
					return false;
				}

				@Override
				public boolean shouldSkipField(FieldAttributes field) {
					return !isFieldAccessible(field);
				}

			}).setPrettyPrinting().create().toJson(plugin));
			writer.close();
		} catch (IOException e) {
			TLog.warn(new TyrantException("Failed to save plugin settings.",
					"The client failed to save the plugin settings for: '" + plugin.getName()
							+ "' during shutdown, giving the following reason: \n" + e.getMessage()));
		}
	}

	public void onLoad() {
		try {
			JsonObject pluginJson = new JsonParser().parse(FileUtils.readFileToString(confFile)).getAsJsonObject();
			for (Field field : plugin.getClass().getDeclaredFields()) {
				if (isFieldAccessible(field)) {
					field.setAccessible(true);
					JsonElement element = pluginJson.get(field.getName());
					if (element != null) {
						field.set(plugin, new Gson().fromJson(element, field.getGenericType()));
					}
				}
			}
		} catch (JsonSyntaxException | IllegalArgumentException | IllegalAccessException | IOException e) {
			TLog.warn(new TyrantException("An exception occurred while loading the plugin '" + plugin.getName() + "'.",
					"The exception: " + e.getMessage()));
		}
	}

	public static boolean isFieldAccessible(Field field) {
		return isFieldAccessible(new FieldAttributes(field));
	}

	public static boolean isFieldAccessible(FieldAttributes field) {
		return field.getAnnotation(Expose.class) != null
				|| (field.hasModifier(Modifier.PUBLIC) && !field.hasModifier(Modifier.FINAL));
	}

}